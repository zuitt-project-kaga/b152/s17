const input = document.getElementById("input");

// ------------------------------------------

let students = ['000'];

function addStudent(name) {
    students.push(name);
    console.log(name + " was added student list.")
}
addStudent("ccc");
addStudent("bbb");
addStudent("aAa");
addStudent("bbb");

// ------------------------------------------

function countStudents() {
    console.log(students.length);
}
countStudents();

// ------------------------------------------

function printStudents() {
    students.sort();
    students.forEach(student => {
        input.innerHTML += `<br> ${student}`;
        console.log(student);
    })
}
printStudents();

// ------------------------------------------

function findStudent(keyWord) {
	let nonCaseSensitiveKeyWord = keyWord.toLowerCase();
	let nonCaseSensitiveStudents = students.map(student => {
		return student.toLowerCase();
	});
    let choosedStudents = nonCaseSensitiveStudents.filter(student => student === nonCaseSensitiveKeyWord);
    if (choosedStudents.length >= 2) {
        console.log('studentNames are enrollees');
    } else if (choosedStudents.length === 1) {
        console.log('studentNames is enrollee');
    } else {
    	console.log('no enrolee');
    }
    console.log(choosedStudents);
}

// ------------------------------------------

// UnitTest

findStudent('Bbb');
findStudent('000');
findStudent('ccc');
findStudent('123');