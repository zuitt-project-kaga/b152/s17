const input = document.getElementById("input");

let student1 = '2020-01-17';
let student2 = '2020-01-20';
let student3 = '2020-01-25';
let student4 = '2020-02-25';

// console.log(student1);
// console.log(student2);
// console.log(student3);

const studentNumbers = ['2020-01-17', '2020-01-20', '2020-01-25', '2020-02-25'];


for (let i = 0; i < studentNumbers.length; i++) {
    console.log(studentNumbers[i]);
}

console.log(studentNumbers[1]);
console.log('index 3 of studentNumbers: ', studentNumbers[3]);

const emptyArray = [];
const grades = [75, 85.5, 92, 94];
const computerBrands = ['Acer', 'Asus', 'Lenovo', 'Apple', 'Redfox', 'Gateway'];

console.log('Empty Array Sample: ', emptyArray);
console.log(grades);
console.log('Computer Brands Array: ', computerBrands);

const tasks = ['drink html', 'eat JS', 'inhale CSS', 'bake sass'];

console.log(tasks);

tasks.push('drum python');
console.log(tasks);

tasks.pop();
console.log(tasks);

tasks.unshift('reactjs', 'angular');
console.log(tasks);

tasks.shift();
console.log(tasks);

tasks.sort();
console.log(tasks);

tasks.reverse();
console.log(tasks);

// --------------

const oddNumbers = [11, 3, 1, 5, 17];
console.log(oddNumbers);

const countries = ['US', 'PH', 'CAN', 'SG', 'PH'];
let indexCountries = countries.indexOf('PH');
console.log(indexCountries);

let lastIndexCountry = countries.lastIndexOf('PH');
console.log(lastIndexCountry);

console.log(countries.toString());

const subTasksA = ['drink html', 'eat js'];
const subTasksB = ['ihale css', 'breathe sass'];
const subTasksC = ['get git', 'be node'];

const subTasks = subTasksA.concat(subTasksB, subTasksC);
console.log(subTasks);

console.log(subTasks.join('-'));
console.log(subTasks.join('@'));
console.log(subTasks.join('x'));

subTasks.forEach((task) => {
    console.log(task);
})

// subTasks.forEach((task)=>{
// 	input.innerHTML += ` ${task} `;
// })

subTasks.forEach((task) => {
    if (task == 'get git') {
        console.log('taskis:  ' + task);
    }
})

const numberList = [1, 2, 3, 4, 5, 6, 7, 8, 9];
numberList.forEach((number) => {
    // input.innerHTML += number;
    if (number % 2 == 0) {
        console.log(number);
    }
})
console.log('number of ' + numberList.length);

const numberMultiple = numberList.map(number => {
    return number * number;
});

console.log(numberMultiple);

// ----------------------------------------------------

const chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
]
console.log(chessBoard);






	// let nonCaseSensitiveKeyWord = keyWord.toLowerCase();
	// nonCaseSensitiveStudents.map((student) => {
	// 	student.toLowerCase();
	// })
	// console.log(nonCaseSensitiveKeyWord);
	// console.log(nonCaseSensitiveStudents);	











// input.innerHTML = '';





// tasks.push('drum python');
// tasks.pop();
// tasks.unshift('reactjs','angular');
// tasks.shift();
// tasks.sort();
// tasks.reverse();
// countries.indexOf('PH');
// countries.lastIndexOf('PH');
// countries.toString();
// subTasksA.concat(subTasksB,subTasksC);
// console.log(subTasks.join('-'));
// subTasks.forEach((task)=>{console.log(task);})
// const numberMultiple = numberList.map(number =>{return number * number;});